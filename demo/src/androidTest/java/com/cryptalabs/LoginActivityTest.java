package com.cryptalabs;

import android.test.ActivityInstrumentationTestCase2;

import com.cryptalabs.db.DatabaseHelper;
import com.cryptalabs.db.MarkerPin;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    private static final double DELTA = 0.0000000001d;
    private LoginActivity activity;

    @SuppressWarnings("deprecation")
    public LoginActivityTest() {
        super("com.cryptalabs", LoginActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
    }

    public void testDbHasEntries() throws SQLException {
        final QueryBuilder<MarkerPin, Integer> queryBuilder = DatabaseHelper.getInstance().getMarkersDao().queryBuilder();
        queryBuilder.clear();
        final List<MarkerPin> markerPins = queryBuilder.distinct().query();
        assertEquals(503, markerPins.size());
    }

    public void testGetDiscreteZoomLevel() {
        int discreteZoomLevel = activity.mapFragment.getDiscreteZoomLevel(0.1f);
        assertEquals(5, discreteZoomLevel);

        discreteZoomLevel = activity.mapFragment.getDiscreteZoomLevel(6.1f);
        assertEquals(5, discreteZoomLevel);

        discreteZoomLevel = activity.mapFragment.getDiscreteZoomLevel(7.9f);
        assertEquals(7, discreteZoomLevel);

        discreteZoomLevel = activity.mapFragment.getDiscreteZoomLevel(12.1f);
        assertEquals(11, discreteZoomLevel);

        discreteZoomLevel = activity.mapFragment.getDiscreteZoomLevel(23.1f);
        assertEquals(17, discreteZoomLevel);
    }

    public void testSelectFromDbForInvalidParameter() {
        final List<MarkerPin> markerPins = activity.mapFragment.selectPinsFromDb(-1);
        assertEquals(0, markerPins.size());
    }

    public void testSelectFromDbFor5() {
        final List<MarkerPin> markerPins = activity.mapFragment.selectPinsFromDb(5);
        assertEquals(53, markerPins.size());
    }

    public void testSelectFromDbFor7() {
        final List<MarkerPin> markerPins = activity.mapFragment.selectPinsFromDb(7);
        assertEquals(36, markerPins.size());
    }

    public void testSelectFromDbEntryFields() {
        final List<MarkerPin> markerPins = activity.mapFragment.selectPinsFromDb(5);
        assertEquals(55.6599916327053d, markerPins.get(0).getLat(), DELTA);
        assertEquals(-2.28815662499999d, markerPins.get(0).getLng(), DELTA);
    }
}