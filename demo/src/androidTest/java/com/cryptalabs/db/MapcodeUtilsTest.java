package com.cryptalabs.db;

import android.test.AndroidTestCase;

import java.util.List;

public class MapcodeUtilsTest extends AndroidTestCase {

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    public void testGetStoredMapcode() throws Exception {
        final List<MarkerPin> storedMapcode = MapcodeUtils.getStoredMapcode();
        assertEquals(4, storedMapcode.size());
        assertTrue(listContainsMarkerPinWithName(storedMapcode, MapcodeUtils.DEFAULT_POINT_1));
        assertTrue(listContainsMarkerPinWithName(storedMapcode, MapcodeUtils.DEFAULT_POINT_2));
        assertTrue(listContainsMarkerPinWithName(storedMapcode, MapcodeUtils.DEFAULT_POINT_3));
        assertTrue(listContainsMarkerPinWithName(storedMapcode, MapcodeUtils.DEFAULT_POINT_4));
    }

    private boolean listContainsMarkerPinWithName(List<MarkerPin> storedMapcode, String markerPinName) {
        for (MarkerPin markerPin : storedMapcode) {
            if (markerPin.getName().equals(markerPinName))
                return true;
        }
        return false;
    }
}