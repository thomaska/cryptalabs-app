package com.cryptalabs.db;

import android.test.AndroidTestCase;

import java.sql.SQLException;

public class DatabaseHelperTest extends AndroidTestCase {

    private static final double DELTA = 0.00001;

    public void testGetMarkerWithName() throws Exception {
        final MarkerPin markerPinGreece = DatabaseHelper.getInstance().queryMarkerWithName("Greece");
        assertEquals("Greece", markerPinGreece.getName());
        assertEquals(5, markerPinGreece.getZoom());
        assertEquals(39.5816504388925d, markerPinGreece.getLat(), DELTA);
        assertEquals(22.1893824375d, markerPinGreece.getLng(), DELTA);
    }

    public void testGetMarkerWithNameForNonExistandName() throws Exception {
        final MarkerPin markerPinNull = DatabaseHelper.getInstance().queryMarkerWithName("BananaLand");
        assertNull(markerPinNull);
    }

    public void testGetMarkerWithNameForNull() throws Exception {
        final MarkerPin markerPinNull = DatabaseHelper.getInstance().queryMarkerWithName(null);
        assertNull(markerPinNull);
    }

    public void testQueryUserWithId() throws Exception {
        User user = new User("id", "hash");
        try {
            DatabaseHelper.getInstance().getUsersDao().create(user);
        } catch (SQLException ignored) {
        }
        final User userWithId = DatabaseHelper.getInstance().queryUserWithId("id");
        assertEquals(user, userWithId);
    }
}