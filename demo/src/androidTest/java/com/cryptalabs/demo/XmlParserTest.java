package com.cryptalabs.demo;

import android.annotation.TargetApi;
import android.os.Build;
import android.test.AndroidTestCase;

import com.cryptalabs.db.MarkerPin;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class XmlParserTest extends AndroidTestCase {

    private static final double DELTA = 0.001;
    private XmlParser xmlParser;
    private final String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
            "<Markers>\n" +
            "  <Point zoom=\"5\" lat=\"55.6599916327053\" long=\"-2.28815662499999\" name=\"UK\" />\n" +
            "  <Point zoom=\"5\" lat=\"53.0488788441064\" long=\"-8.44050037499999\" name=\"Ireland\" />\n" +
            "</Markers>";
    private InputStream stream;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void setUp() throws Exception {
        super.setUp();
        xmlParser = new XmlParser();
        stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    }

    public void testParse() throws Exception {
        final List parseResult = xmlParser.parse(stream);
        assertEquals(2, parseResult.size());
        assertEquals(55.6599916327053, ((MarkerPin) parseResult.get(0)).getLat(), DELTA);
        assertEquals(-2.28815662499999, ((MarkerPin) parseResult.get(0)).getLng(), DELTA);
        assertEquals("Ireland", ((MarkerPin) parseResult.get(1)).getName());
        assertEquals(5, ((MarkerPin) parseResult.get(1)).getZoom());
    }
}