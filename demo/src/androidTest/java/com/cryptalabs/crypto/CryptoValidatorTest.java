package com.cryptalabs.crypto;

import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.cryptalabs.db.MarkerPin;

import java.util.ArrayList;

public class CryptoValidatorTest extends AndroidTestCase {

    private MarkerPin markerPin;

    @Override
    public void setUp() {
        markerPin = new MarkerPin(11, "Name", 34d, 34d);
    }

    @SmallTest
    public void testValidateSecondNullParameter() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        storedMapcode.add(markerPin);
        assertFalse(CryptoValidator.validate(new ArrayList<MarkerPin>(), storedMapcode));
    }

    @SmallTest
    public void testValidateFirstNullParameter() {
        final ArrayList<MarkerPin> markerPins = new ArrayList<MarkerPin>();
        markerPins.add(markerPin);
        assertFalse(CryptoValidator.validate(markerPins, null));
    }

    @SmallTest
    public void testValidateEmptyList() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        final ArrayList<MarkerPin> mapcodeToValidate = new ArrayList<MarkerPin>();
        storedMapcode.add(markerPin);
        mapcodeToValidate.add(markerPin);
        mapcodeToValidate.add(markerPin);
        assertFalse(CryptoValidator.validate(mapcodeToValidate, storedMapcode));
    }

    @SmallTest
    public void testValidateListSingleElement() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        final ArrayList<MarkerPin> mapcodeToValidate = new ArrayList<MarkerPin>();
        storedMapcode.add(markerPin);
        mapcodeToValidate.add(markerPin);
        assertTrue(CryptoValidator.validate(mapcodeToValidate, storedMapcode));
    }

    @SmallTest
    public void testValidateSuccessScenario() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        final ArrayList<MarkerPin> mapcodeToValidate = new ArrayList<MarkerPin>();

        MarkerPin markerPin1 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin2 = new MarkerPin(12, "Test", 12d, 13d);

        MarkerPin markerPin3 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin4 = new MarkerPin(12, "Test", 12d, 13d);

        mapcodeToValidate.add(markerPin1);
        mapcodeToValidate.add(markerPin2);

        storedMapcode.add(markerPin3);
        storedMapcode.add(markerPin4);

        assertTrue(CryptoValidator.validate(mapcodeToValidate, storedMapcode));
    }


    @SmallTest
    public void testValidateMarkersDifferentName() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        final ArrayList<MarkerPin> mapcodeToValidate = new ArrayList<MarkerPin>();

        MarkerPin markerPin1 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin2 = new MarkerPin(12, "Test", 12d, 13d);

        MarkerPin markerPin3 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin4 = new MarkerPin(12, "Test1", 12d, 13d);

        mapcodeToValidate.add(markerPin1);
        mapcodeToValidate.add(markerPin2);

        storedMapcode.add(markerPin3);
        storedMapcode.add(markerPin4);

        assertFalse(CryptoValidator.validate(mapcodeToValidate, storedMapcode));
    }

    @SmallTest
    public void testValidateMarkersDifferentLat() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        final ArrayList<MarkerPin> mapcodeToValidate = new ArrayList<MarkerPin>();

        MarkerPin markerPin1 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin2 = new MarkerPin(12, "Test", 12d, 13d);

        MarkerPin markerPin3 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin4 = new MarkerPin(12, "Test", 11d, 13d);

        mapcodeToValidate.add(markerPin1);
        mapcodeToValidate.add(markerPin2);

        storedMapcode.add(markerPin3);
        storedMapcode.add(markerPin4);

        assertFalse(CryptoValidator.validate(mapcodeToValidate, storedMapcode));
    }

    @SmallTest
    public void testValidateMarkersDifferentLng() {
        final ArrayList<MarkerPin> storedMapcode = new ArrayList<MarkerPin>();
        final ArrayList<MarkerPin> mapcodeToValidate = new ArrayList<MarkerPin>();

        MarkerPin markerPin1 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin2 = new MarkerPin(12, "Test", 12d, 13d);

        MarkerPin markerPin3 = new MarkerPin(11, "Name", 34d, 34d);
        MarkerPin markerPin4 = new MarkerPin(12, "Test", 12d, 11d);

        mapcodeToValidate.add(markerPin1);
        mapcodeToValidate.add(markerPin2);

        storedMapcode.add(markerPin3);
        storedMapcode.add(markerPin4);

        assertFalse(CryptoValidator.validate(mapcodeToValidate, storedMapcode));
    }
}