package com.cryptalabs;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;

import com.cryptalabs.db.MarkerPin;
import com.google.android.gms.maps.model.Marker;

public class CustomMapFragmentTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    private LoginActivity activity;
    private CustomMapFragment mapFragment;

    @SuppressWarnings("deprecation")
    public CustomMapFragmentTest() {
        super("com.cryptalabs", LoginActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        mapFragment = activity.mapFragment;
    }

    @UiThreadTest
    public void testAddMarker() throws Exception {
        MarkerPin markerPin = new MarkerPin(11, "Name", 10d, 20d);
        final Marker marker = mapFragment.addMarker(markerPin);
        assertEquals(markerPin.getZoom() + "", marker.getSnippet());
        assertEquals(markerPin.getName(), marker.getTitle());
    }
}