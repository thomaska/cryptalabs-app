package com.cryptalabs.crypto;

import android.util.Base64;

public class StringXORer {

    public String encode(String s, String key) {
        return base64Encode(xorWithKey(s.getBytes(), key.getBytes()));
    }
    
    public String encodeS(String s, String key) {
        return new String(xorWithKey(s.getBytes(), key.getBytes()));
    }

    public String decode(String s, String key) {
        return new String(xorWithKey(base64Decode(s), key.getBytes()));
    }

    private byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i%key.length]);
        }
        return out;
    }

    private byte[] base64Decode(String s) {
    	//BASE64Decoder d = new BASE64Decoder();
    	byte[] data = Base64.decode(s, Base64.DEFAULT);

    	return data;
    }

    private String base64Encode(byte[] bytes) {
        //BASE64Encoder enc = new BASE64Encoder();
    	String base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        return base64.replaceAll("\\s", "");

    }
}
