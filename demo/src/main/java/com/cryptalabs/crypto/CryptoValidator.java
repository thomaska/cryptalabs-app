package com.cryptalabs.crypto;

import com.cryptalabs.db.DatabaseHelper;
import com.cryptalabs.db.MapcodeUtils;
import com.cryptalabs.db.MarkerPin;
import com.cryptalabs.db.User;
import com.cryptalabs.login.ErrorLoginHandler;
import com.cryptalabs.login.ILoginHandler;
import com.cryptalabs.login.RegularLoginHandler;
import com.cryptalabs.login.ResetHandler;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class CryptoValidator {


    public static boolean validate(List<MarkerPin> mapcodeToVerify, List<MarkerPin> storedMapcode) {

        if (mapcodeToVerify == null || storedMapcode == null)
            return false;
        if (mapcodeToVerify.size() != storedMapcode.size())
            return false;

        return getHash(mapcodeToVerify).equals(getHash(storedMapcode));
    }

    public static ILoginHandler validate(String name, List<MarkerPin> mapcodeToVerify) {
        if (mapcodeToVerify == null || mapcodeToVerify.size() < 4)
            return new ErrorLoginHandler();
        else {
            final User user = DatabaseHelper.getInstance().queryUserWithId(name);
            if (user != null && getHash(mapcodeToVerify).equals(user.getHash()))
                return new RegularLoginHandler();
            else if (user == null && validate(mapcodeToVerify, MapcodeUtils.getStoredMapcode()))
                return new ResetHandler();
        }
        return new ErrorLoginHandler();

    }

    public static String getHash(List<MarkerPin> mapcode) {
        int numberOfPoints = mapcode.size();
        String[] shaMapcode = new String[numberOfPoints];

        for (int i = 0; i < numberOfPoints; i++) {
            String mapcodeToVerifyStr = Double.toString(mapcode.get(i).getLat()) + Double.toString(mapcode.get(i).getLng()) + mapcode.get(i).getName();
            shaMapcode[i] = sha256(mapcodeToVerifyStr);
        }

        String finalHash = shaMapcode[0];
        StringXORer obj = new StringXORer();
        for (int i = 1; i < numberOfPoints; i++) {
            finalHash = obj.encodeS(finalHash, shaMapcode[i]);
        }
        return finalHash;
    }

    private static String sha256(String value) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("SHA-256 is not found: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        md.update(value.getBytes());

        byte byteData[] = md.digest();

        StringBuilder sb = new StringBuilder();
        for (byte aByteData : byteData) {
            sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
