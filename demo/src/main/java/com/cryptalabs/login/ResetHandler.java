package com.cryptalabs.login;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.cryptalabs.R;
import com.cryptalabs.ResetPasswordActivity;

public class ResetHandler implements ILoginHandler {
    @Override
    public void hadle(Activity activity) {
        activity.startActivity(new Intent(activity, ResetPasswordActivity.class));
        Toast.makeText(activity, activity.getString(R.string.login_successful), Toast.LENGTH_LONG).show();
        activity.finish();
    }
}
