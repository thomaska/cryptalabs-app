package com.cryptalabs.login;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.cryptalabs.SecondFactorAuthenticatorActivity;

public class RegularLoginHandler implements ILoginHandler {
    @Override
    public void hadle(Activity activity) {
        activity.startActivity(new Intent(activity, SecondFactorAuthenticatorActivity.class));
        Toast.makeText(activity, "Success", Toast.LENGTH_LONG).show();
        activity.finish();
    }
}
