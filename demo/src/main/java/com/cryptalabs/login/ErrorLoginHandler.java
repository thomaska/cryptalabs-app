package com.cryptalabs.login;

import android.app.Activity;
import android.widget.Toast;

import com.cryptalabs.R;

public class ErrorLoginHandler implements ILoginHandler {
    @Override
    public void hadle(Activity activity) {
        Toast.makeText(activity, activity.getString(R.string.wrong_mapcode_try_again), Toast.LENGTH_LONG).show();
    }
}
