package com.cryptalabs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class SecondFactorAuthenticatorActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_factor_authenticator);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        long lastTime;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final Random random = new Random(System.currentTimeMillis());

            final View rootView = inflater.inflate(R.layout.fragment_second_factor_authenticator, container, false);
            final ProgressBar mProgress = (ProgressBar) rootView.findViewById(R.id.progress_horizontal);
            final TextView randomNumber = (TextView) rootView.findViewById(R.id.tv_random);
            final EditText numberTocheck = (EditText) rootView.findViewById(R.id.txt_secondfactor);
            final Button okBtn = (Button) rootView.findViewById(R.id.okBtn);

            // Start lengthy operation in a background thread
            final Handler mHandler = new Handler();
            new Thread(new Runnable() {
                public static final long THIRTY_SEC = 20000;

                public void run() {
                    lastTime = System.currentTimeMillis();
                    while (true) {
                        // Update the progress bar
                        final long timeElapsed = System.currentTimeMillis() - lastTime;
                        mHandler.post(new Runnable() {
                            public void run() {
                                mProgress.setProgress((int) (timeElapsed / 200));
                                if (timeElapsed > THIRTY_SEC) {
                                    randomNumber.setText("" + (int) (random.nextFloat() * 1000000));
                                }
                            }
                        });
                        if (timeElapsed > THIRTY_SEC) {
                            lastTime = System.currentTimeMillis();
                        }
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();

            setupOkBtn(okBtn, numberTocheck, randomNumber);
            return rootView;
        }

        private void setupOkBtn(Button okBtn, final EditText numTocheck, final TextView correctNum) {
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (correctNum.getText().toString().equals(numTocheck.getText().toString())) {
                        Toast.makeText(getActivity(), "Login Successfull", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getActivity(), RegularLoginActivity.class));
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), "Please enter the correct number", Toast.LENGTH_LONG).show();
                    }

                }
            });
        }
    }
}
