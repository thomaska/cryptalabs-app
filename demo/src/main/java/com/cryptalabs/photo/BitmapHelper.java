package com.cryptalabs.photo;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapHelper {
    private static final String TAG = BitmapHelper.class.getName();

    public static void recycleBitmap(Bitmap picture) {
        if (picture != null && !picture.isRecycled()) picture.recycle();
    }

    public static Bitmap rotateBitmap(int angle, Bitmap picture) {
        Matrix m = new Matrix();
        m.postRotate(angle);
        Bitmap tempPicture = picture;
        picture = Bitmap.createBitmap(picture, 0, 0, picture.getWidth(),
                picture.getHeight(), m, false);

        recycleBitmap(tempPicture);
        return picture;
    }

    public static void saveBitmapToFile(Bitmap b, String filePath) {
        File f = new File(filePath);
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(f);
            b.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();

        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
