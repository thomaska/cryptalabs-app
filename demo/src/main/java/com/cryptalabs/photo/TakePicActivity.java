package com.cryptalabs.photo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.cryptalabs.R;
import com.cryptalabs.SecondFactorAuthenticatorActivity;
import com.polites.android.GestureImageView;

import static com.cryptalabs.photo.BitmapHelper.rotateBitmap;
import static com.cryptalabs.photo.BitmapHelper.saveBitmapToFile;

public class TakePicActivity extends ActionBarActivity {
    GestureImageView photo;
    FrameLayout cameraPreview;
    Button capture;
    Button retake;
    Button confirm;

    private Bitmap picture;
    private Camera camera;
    private CameraPreview preview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_pick);
        initViews();

        final String filePath = makeTargetPhotoPath();
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TakePicActivity.this, TakePicActivity.class);
                startActivity(intent);
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap b = photo.getBitmapFromView();

                saveBitmapToFile(b, filePath);
                final ProgressDialog dialog = ProgressDialog.show(TakePicActivity.this, "Please wait...", "Setting up two factor authentication");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                            startActivity(new Intent(TakePicActivity.this, SecondFactorAuthenticatorActivity.class));
                            finish();
                        }
                    }
                }, 4000);
            }
        });

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturePicture();
            }
        });
    }

    private void initViews() {
        photo = (GestureImageView) findViewById(R.id.ivPhoto);
        cameraPreview = (FrameLayout) findViewById(R.id.flCameraPreview);
        capture = (Button) findViewById(R.id.bCapture);
        retake = (Button) findViewById(R.id.bRetake);
        confirm = (Button) findViewById(R.id.bConfirm);
    }

    public static String makeTargetPhotoPath() {
        return "/sdcard/picture" + System.currentTimeMillis() + ".jpg";
    }

    @Override
    protected void onResume() {
        super.onResume();
        initCamera();
    }

    @Override
    protected void onPause() {
        if (camera != null) camera.release();
        BitmapHelper.recycleBitmap(picture);
        super.onPause();
    }

    private void initCamera() {
        final boolean success = getCameraInstance();
        if (!success)
            return;
        CameraPreview.setCameraDisplayOrientation(this, 0, camera);
        // Create our Preview view and set it as the content of our activity.
        preview = new CameraPreview(this, camera);
        cameraPreview.addView(preview);
    }

    private void capturePicture() {
        camera.takePicture(null, null, mPicture);
    }

    /**
     * @return camera or null if camera is unavailable
     */
    public boolean getCameraInstance() {
        if (camera != null) return true;
        try {
            camera = Camera.open();
            return true;
        } catch (Exception e) {
            Toast.makeText(this, "No camera detected", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private PictureCallback mPicture = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, final Camera camera) {

            picture = BitmapFactory.decodeByteArray(data, 0, data.length);
            int rotation = TakePicActivity.this.getWindowManager().getDefaultDisplay()
                    .getRotation();

            switch (rotation) {
                case Surface.ROTATION_0:
                    picture = rotateBitmap(90, picture);
                    break;
            }

            removePreviewAndShowPicture();
            prepareButtonsForTakenPicture();

            //new DetectFacesInBackground(FacePickActivity.this, picture).execute();
            //progressView.setVisibility(View.VISIBLE);
        }
    };

    private void removePreviewAndShowPicture() {
        cameraPreview.removeAllViews();
        photo.setImageBitmap(picture);
        photo.setVisibility(View.VISIBLE);
        photo.postInvalidate();
    }

    private void prepareButtonsForTakenPicture() {
        capture.setVisibility(View.GONE);
        confirm.setVisibility(View.VISIBLE);
        retake.setVisibility(View.VISIBLE);
    }
}
