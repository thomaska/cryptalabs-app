package com.cryptalabs.utils;


import android.content.Context;
import android.content.SharedPreferences;

import com.cryptalabs.R;
import com.cryptalabs.demo.DemoApp;

public class CryptaPrefs {

    private static final String PROPERTY_DB_INSTALLED = "registration_id";

    private static final Context context = DemoApp.getContext();
    private static final SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);

    public static void setDbInstalled(boolean installed) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PROPERTY_DB_INSTALLED, installed);
        editor.commit();
    }

    public static boolean isDbInstalled() {
        return prefs.getBoolean(PROPERTY_DB_INSTALLED, false);
    }
}
