package com.cryptalabs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cryptalabs.db.DatabaseHelper;
import com.cryptalabs.db.MarkerPin;
import com.cryptalabs.db.MarkerPinNameZoom;
import com.cryptalabs.otto.BusProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.google.android.gms.common.GooglePlayServicesUtil.getErrorDialog;

public class CustomMapFragment extends SupportMapFragment implements GoogleMap.OnMarkerClickListener {


    private static final String TAG = CustomMapFragment.class.getSimpleName();
    private BitmapDescriptor icon;

    public CustomMapFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {
        View v = super.onCreateView(inflater, group, bundle);
        initialiseMap();
        icon = BitmapDescriptorFactory.fromResource(R.drawable.pin);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity.getApplicationContext());
        if (status != ConnectionResult.SUCCESS) {
            Dialog dialog = getErrorDialog(status, getActivity(), 10);
            dialog.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        final GoogleMap map = getMap();
        if (map != null) {
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                private float currentZoom = -1;

                @Override
                public void onCameraChange(CameraPosition pos) {
                    if (pos.zoom != currentZoom) {
                        currentZoom = pos.zoom;
                        map.clear();
                        int discreteZoomLevel = getDiscreteZoomLevel(currentZoom);
                        insertMarkersForZoomLevel(discreteZoomLevel);
                    }
                }
            });
        }
    }

    int getDiscreteZoomLevel(float currentZoom) {
        if (currentZoom > 17)
            return 17;
        else if (currentZoom > 11)
            return 11;
        else if (currentZoom > 7)
            return 7;
        else return 5;
    }

    private void insertMarkersForZoomLevel(int zoom) {
        final List<MarkerPin> markerPins = selectPinsFromDb(zoom);
        for (MarkerPin markerPin : markerPins) {
            addMarker(markerPin);
        }
    }

    List<MarkerPin> selectPinsFromDb(int zoom) {
        try {
            final QueryBuilder<MarkerPin, Integer> queryBuilder = DatabaseHelper.getInstance().getMarkersDao().queryBuilder();
            return queryBuilder.where().eq("zoom", zoom).query();
        } catch (SQLException e) {
            Log.e(TAG, "insertMarkersForZoomLevel: Exception in querying db - " + e.getMessage());
            e.printStackTrace();
        }
        return new ArrayList<MarkerPin>();
    }

    Marker addMarker(MarkerPin markerPin) {
        if (markerPin == null) {
            return null;
        }
        LatLng position = new LatLng(markerPin.getLat(), markerPin.getLng());
        MarkerOptions markerOptions = new MarkerOptions().position(position).title(markerPin.getName()).anchor(0.5f, 0.5f).icon(icon);
        markerOptions.snippet(markerPin.getZoom() + "");
        final GoogleMap map = getMap();
        if (map != null) {
            return map.addMarker(markerOptions);
        }
        return null;
    }

    private void initialiseMap() {
        getMap().setMapType(GoogleMap.MAP_TYPE_NORMAL);
        getMap().setMyLocationEnabled(false);
        getMap().setOnMarkerClickListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }


    @Override
    public boolean onMarkerClick(final Marker marker) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pin_highlight);
        marker.setIcon(icon);
        int zoom = Integer.parseInt(marker.getSnippet());
        MarkerPinNameZoom markerPin = new MarkerPinNameZoom(marker.getTitle(), zoom);
        BusProvider.getInstance().post(markerPin);
//        Toast.makeText(getActivity(), "Marker: " + marker.getTitle() + " Long:" + marker.getPosition().longitude + " Lat:" + marker.getPosition().latitude, Toast.LENGTH_LONG).show();
        return true;
    }
}
