package com.cryptalabs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class RegularLoginActivity extends ActionBarActivity {

    private static final String TAG = RegularLoginActivity.class.getName();
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regular_login_layout);
        WebView cryptalabsWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = cryptalabsWebView.getSettings();
        webSettings.setJavaScriptEnabled(false);
        webSettings.setBuiltInZoomControls(true);
        cryptalabsWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        progressBar = ProgressDialog.show(RegularLoginActivity.this, getString(R.string.login_successful), getString(R.string.loading__));
        progressBar.setCancelable(true);
        cryptalabsWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " + url);
                if (progressBar != null && progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
                Toast.makeText(RegularLoginActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                alertDialog.setTitle("Error");
                alertDialog.setMessage(description);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int which) {
                    }
                });
                alertDialog.show();
            }
        });

        cryptalabsWebView.loadUrl("http://www.cryptalabs.com");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }
}
