package com.cryptalabs.demo;

import android.app.Application;
import android.content.Context;

public class DemoApp extends Application {


    private static Context context;
    private static String currentUserId;

    public static void setCurrentUserId(String currentUserId) {
        DemoApp.currentUserId = currentUserId;
    }

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static String getCurrentUserId() {
        return currentUserId;
    }

    public static Context getContext() {
        return context;
    }
}
