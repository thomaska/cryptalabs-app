package com.cryptalabs.demo;

import android.util.Xml;

import com.cryptalabs.db.MarkerPin;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XmlParser {

    private static final String ns = null;

    public List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<MarkerPin> entries = new ArrayList<MarkerPin>();

        parser.require(XmlPullParser.START_TAG, ns, "Markers");
        while (parser.next() != XmlPullParser.END_TAG) {
            final MarkerPin pin = readEntry(parser);
            if (pin != null)
                entries.add(pin);
            else return entries;
        }
        return entries;
    }


    private MarkerPin readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Point")) {
                return readPoint(parser);
            }
        }
        return null;
    }

    private MarkerPin readPoint(XmlPullParser parser) throws IOException, XmlPullParserException {
        final int zoom = Integer.parseInt(parser.getAttributeValue(null, "zoom"));
        final double lat = Double.parseDouble(parser.getAttributeValue(null, "lat"));
        final double lng = Double.parseDouble(parser.getAttributeValue(null, "long"));
        final String name = parser.getAttributeValue(null, "name");
        parser.next();
        return new MarkerPin(zoom, name, lat, lng);
    }
}
