package com.cryptalabs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cryptalabs.crypto.CryptoValidator;
import com.cryptalabs.db.DatabaseHelper;
import com.cryptalabs.db.MarkerPin;
import com.cryptalabs.db.MarkerPinNameZoom;
import com.cryptalabs.demo.DemoApp;
import com.cryptalabs.login.ILoginHandler;
import com.cryptalabs.otto.BusProvider;
import com.cryptalabs.otto.events.DataInsertionFinished;
import com.cryptalabs.utils.CryptaPrefs;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends ActionBarActivity {

    private ProgressDialog dialog;
    CustomMapFragment mapFragment;
    private List<MarkerPin> markerPins = new ArrayList<MarkerPin>();
    private TextView numberOfSelectedPinsTV;
    private int numberOfSelectedPins;
    private Button loginBtn;
    private EditText userid;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!CryptaPrefs.isDbInstalled()) {
            dialog = ProgressDialog.show(LoginActivity.this, getString(R.string.please_wait), getString(R.string.loading_data));
            startService(new Intent(LoginActivity.this, InsertDataToDbIntentService.class));
            CryptaPrefs.setDbInstalled(true);
        }
        mapFragment = (CustomMapFragment) getSupportFragmentManager().findFragmentById(R.id.customMapFragment);
        numberOfSelectedPinsTV = (TextView) findViewById(R.id.tv_number_of_pins);
        userid = (EditText) findViewById(R.id.txt_username);
        setupLoginButton();
    }

    private void setupLoginButton() {
        loginBtn = (Button) findViewById(R.id.btn_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String useridText = userid.getText().toString();
                if (useridText == null || useridText.equals("")) {
                    Toast.makeText(LoginActivity.this, getString(R.string.please_enter_a_bank_account), Toast.LENGTH_LONG).show();
                    return;
                }
                if (numberOfSelectedPins != 4) {
                    Toast.makeText(LoginActivity.this, getString(R.string.login_btn_msg), Toast.LENGTH_LONG).show();
                    return;
                }
                DemoApp.setCurrentUserId(useridText);
                final ILoginHandler iLoginHandler = CryptoValidator.validate(useridText, markerPins);
                iLoginHandler.hadle(LoginActivity.this);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_clear) {
            clearMarkerSelection();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clearMarkerSelection() {
        Toast.makeText(LoginActivity.this, getString(R.string.marker_selection_cleared), Toast.LENGTH_LONG).show();
        markerPins.clear();
        numberOfSelectedPins = 0;
        numberOfSelectedPinsTV.setText("0");
        loginBtn.setEnabled(false);
    }

    @Subscribe
    public void onInsertDataToDbFinished(DataInsertionFinished event) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Subscribe
    public void onMarkerClicked(MarkerPinNameZoom markerPinNameZoom) {
        MarkerPin markerPin = DatabaseHelper.getInstance().queryMarkerWithName(markerPinNameZoom.getName());
        markerPins.add(markerPin);
        numberOfSelectedPinsTV.setText(++numberOfSelectedPins + "");
        if (numberOfSelectedPins == 4)
            loginBtn.setEnabled(true);
    }
}