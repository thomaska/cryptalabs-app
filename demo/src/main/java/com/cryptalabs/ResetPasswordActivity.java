package com.cryptalabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cryptalabs.crypto.CryptoValidator;
import com.cryptalabs.db.DatabaseHelper;
import com.cryptalabs.db.MarkerPin;
import com.cryptalabs.db.MarkerPinNameZoom;
import com.cryptalabs.db.User;
import com.cryptalabs.demo.DemoApp;
import com.cryptalabs.otto.BusProvider;
import com.cryptalabs.photo.TakePicActivity;
import com.squareup.otto.Subscribe;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ResetPasswordActivity extends ActionBarActivity implements View.OnClickListener {

    private static final Configuration CONFIGURATION_INFINITE = new Configuration.Builder()
            .setDuration(Configuration.DURATION_INFINITE)
            .build();
    private Crouton crouton;
    private TextView numberOfSelectedPinsTV;
    private List<MarkerPin> markerPins = new ArrayList<MarkerPin>();
    private List<MarkerPin> confirmPins = new ArrayList<MarkerPin>();

    private int numberOfSelectedPins;
    private boolean confirm;
    private Button set;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);
        crouton = Crouton.makeText(this, "Please set your mapcode by selecting 4 markers.", Style.INFO);
        crouton.setOnClickListener(this).setConfiguration(CONFIGURATION_INFINITE).show();

        set = (Button) findViewById(R.id.btn_set_mapcode);
        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (numberOfSelectedPins != 4) {
                    Toast.makeText(ResetPasswordActivity.this, getString(R.string.login_btn_msg), Toast.LENGTH_LONG).show();
                    return;
                }
                if (!confirm) {
                    confirm = true;
                    set.setText(getString(R.string.confirm));
                    numberOfSelectedPins = 0;
                    numberOfSelectedPinsTV.setText("0");
                } else {
                    final boolean validate = CryptoValidator.validate(markerPins, confirmPins);
                    if (validate) {
                        Toast.makeText(ResetPasswordActivity.this, getString(R.string.please_take_a_photo), Toast.LENGTH_LONG).show();
                        try {
                            final User user = new User(DemoApp.getCurrentUserId(), CryptoValidator.getHash(markerPins));
                            DatabaseHelper.getInstance().getUsersDao().create(user);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        startActivity(new Intent(ResetPasswordActivity.this, TakePicActivity.class));
                        finish();
                    } else {
                        Toast.makeText(ResetPasswordActivity.this, getString(R.string.the_mapcodes_do_not_match), Toast.LENGTH_LONG).show();
                        resetUIElems();
                    }
                }
            }
        });
        numberOfSelectedPinsTV = (TextView) findViewById(R.id.tv_number_of_pins);

    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        resetUIElems();
    }

    @Override
    public void onClick(View v) {
        Crouton.hide(crouton);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_clear) {
            clearMarkerSelection();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void resetUIElems() {
        numberOfSelectedPins = 0;
        numberOfSelectedPinsTV.setText("0");
        confirm = false;
        markerPins.clear();
        confirmPins.clear();
        set.setText(getString(R.string.ok));
    }

    private void clearMarkerSelection() {
        Toast.makeText(ResetPasswordActivity.this, getString(R.string.marker_selection_cleared), Toast.LENGTH_LONG).show();
        if (confirm)
            confirmPins.clear();
        else
            markerPins.clear();
        numberOfSelectedPins = 0;
        numberOfSelectedPinsTV.setText("0");
    }


    @Subscribe
    public void onMarkerClicked(MarkerPinNameZoom markerPinNameZoom) {
        MarkerPin markerPin = DatabaseHelper.getInstance().queryMarkerWithName(markerPinNameZoom.getName());
        if (confirm)
            confirmPins.add(markerPin);
        else
            markerPins.add(markerPin);
        numberOfSelectedPinsTV.setText(++numberOfSelectedPins + "");
    }
}
