package com.cryptalabs;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.cryptalabs.db.DatabaseHelper;
import com.cryptalabs.db.MarkerPin;
import com.cryptalabs.demo.XmlParser;
import com.cryptalabs.otto.BusProvider;
import com.cryptalabs.otto.events.DataInsertionFinished;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class InsertDataToDbIntentService extends IntentService {
    private static final String TAG = InsertDataToDbIntentService.class.getSimpleName();

    public InsertDataToDbIntentService() {
        super("InsertDataToDbIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            insertDataInDb();
            BusProvider.getInstance().post(new DataInsertionFinished());
        }
    }

    private void insertDataInDb() {
        final XmlParser xmlParser = new XmlParser();
        try {
            @SuppressWarnings("unchecked")
            List<MarkerPin> markerPins = xmlParser.parse(this.getResources().openRawResource(R.raw.markers));
            for (MarkerPin pin : markerPins)
                DatabaseHelper.getInstance().getMarkersDao().createIfNotExists(pin);
        } catch (XmlPullParserException e) {
            Log.e(TAG, "insertDataInDb: XmlPullParserException:" + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "insertDataInDb: IOException:" + e.getMessage());
            e.printStackTrace();
        } catch (SQLException e) {
            Log.e(TAG, "insertDataInDb: SQLException:" + e.getMessage());
            e.printStackTrace();
        }
    }
}
