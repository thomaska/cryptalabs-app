package com.cryptalabs.db;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final String path = "/Users/thomaska/Documents/Android Studio workspace/Cryptalabs/demo/src/main/res/raw/ormlite_config.txt";
    private static final Class<?>[] classes = new Class[]{
            MarkerPin.class,
            User.class
    };

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile(new File(path), classes);
    }
}