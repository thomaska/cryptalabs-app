package com.cryptalabs.db;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;
import java.util.List;

public class MapcodeUtils {

    private static final String TAG = MapcodeUtils.class.getName();
    public static final String DEFAULT_POINT_1 = "France";
    public static final String DEFAULT_POINT_2 = "Italy";
    public static final String DEFAULT_POINT_3 = "Greece";
    public static final String DEFAULT_POINT_4 = "Serbia";

    public static List<MarkerPin> getStoredMapcode() {
        try {
            final Dao<MarkerPin, Integer> markersDao = DatabaseHelper.getInstance().getMarkersDao();
            final QueryBuilder<MarkerPin, Integer> queryBuilder = markersDao.queryBuilder();
            Where where = queryBuilder.where();
            where.or(
                    where.eq(MarkerPin.NAME_TAG, DEFAULT_POINT_1),
                    where.eq(MarkerPin.NAME_TAG, DEFAULT_POINT_2),
                    where.eq(MarkerPin.NAME_TAG, DEFAULT_POINT_3),
                    where.eq(MarkerPin.NAME_TAG, DEFAULT_POINT_4));
            return queryBuilder.distinct().query();
        } catch (SQLException e) {
            Log.e(TAG, "getStoredMapcode - Exception:" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
