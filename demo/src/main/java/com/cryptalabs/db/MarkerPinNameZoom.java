package com.cryptalabs.db;

public class MarkerPinNameZoom {
    private final int zoom;
    private final String name;

    public MarkerPinNameZoom(String title, int snippet) {
        this.name = title;
        this.zoom = snippet;
    }

    public int getZoom() {
        return zoom;
    }

    public String getName() {
        return name;
    }
}
