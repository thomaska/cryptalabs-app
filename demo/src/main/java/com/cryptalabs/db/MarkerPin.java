package com.cryptalabs.db;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "markerpin")
public class MarkerPin implements Parcelable {
    public static final String NAME_TAG = "name";
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField
    private int zoom;
    @DatabaseField
    private String name;
    @DatabaseField
    private double lat;
    @DatabaseField
    private double lng;

    public MarkerPin() {
    }

    public MarkerPin(int zoom, String name, double lat, double lng) {
        this.zoom = zoom;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public MarkerPin createFromParcel(Parcel in) {
            return new MarkerPin(in);
        }

        public MarkerPin[] newArray(int size) {
            return new MarkerPin[size];
        }
    };

    public MarkerPin(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(zoom);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeString(name);
    }

    private void readFromParcel(Parcel in) {
        id = in.readLong();
        zoom = in.readInt();
        lat = in.readDouble();
        lng = in.readDouble();
        name = in.readString();
    }

    public long getId() {
        return id;
    }

    public int getZoom() {
        return zoom;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
